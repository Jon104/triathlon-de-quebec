-[![Netlify Status](https://api.netlify.com/api/v1/badges/fc4759d5-f38a-42bb-886a-3933527079dc/deploy-status)](https://app.netlify.com/sites/triathlondequebec/deploys)

# Triathlon de Québec

Site en prod : <https://www.triathlondequebec.com/>

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

```sh
nvm use
yarn install
yarn start
```
