import {Paper, Theme, Typography} from "@material-ui/core";
import {withStyles} from "@material-ui/core/styles";
import React from "react";
import { t } from "../../../locales";

interface MyProps {
    classes: any;
}

const styles = (theme: Theme) => ({
    paper: {
        color: theme.palette.text.secondary,
        padding: theme.spacing(2),
    },
    typography: {
        padding: "20px",
    },
});

class AmbassadeurJoanie extends React.Component<MyProps> {
    public render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.paper}>
                <Typography variant="subtitle1">{t("ambassadors.joanie")}</Typography>
                <Typography variant="body2" className="basic-padding-bottom">{t("ambassadors.joanieTitle")}</Typography>
                <img
                    src={require("../../../assets/joanie.jpg")}
                    alt="image Joanie Fortin"
                    className="parcours-ste-foy basic-padding-bottom"
                />
                <Typography variant="body1">
                    {t("ambassadors.joanieText")}
                </Typography>
            </Paper>
        );
    }
}

export default withStyles(styles)(AmbassadeurJoanie);
