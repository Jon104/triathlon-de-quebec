import {
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary,
    Typography,
    withStyles,
} from "@material-ui/core";
import React from "react";
import {t} from "../../../locales";
import BaieDeBeauportDescription from "./BaieDeBeauportDescription";
import GuideAthlete from "./GuideAthlete";
import HoraireBaieDeBeauport from "./HoraireBaieDeBeauport";
import InfoCovid from "./InfoCovid";
import ParcoursBaieDeBeauport from "./ParcoursBaieDeBeauport";
import PlanDuSite from "./PlanDuSite";
import ReunionAvantCourse from "./ReunionAvantCourse";
import TarifsBeauport from "./TarifsBaieDeBeauport";

const styles = {
    listItem: {
        padding: "0",
    },
    specialAnnouncement: {
        backgroundColor: "#a90409",
        color: "white",
    },
    specialAnnouncementText: {
        color: "white",
    },
};

interface MyProps {
    classes: any;
}

class DrawerBaieDeBeauportContent extends React.Component<MyProps> {

    public render() {
        const {classes} = this.props;

        return (
            <>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("beauport_bay.description")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <BaieDeBeauportDescription />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("beauport_bay.rate")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <TarifsBeauport/>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("beauport_bay.schedule")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <HoraireBaieDeBeauport/>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("beauport_bay.course")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <ParcoursBaieDeBeauport/>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">Plans du site</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <PlanDuSite/>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">Guide de l'athlète</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <GuideAthlete />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">Réunion d'avant course 📺</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <ReunionAvantCourse />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">Responsabilité et COVID-19 🦠😷</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <InfoCovid />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("beauport_bay.results")} 🏆</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography variant="h6">À venir</Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">Foire au question</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography variant="h6">À venir</Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </>
        );
    }
}

export default withStyles(styles, {withTheme: true})(DrawerBaieDeBeauportContent);
