import {Typography} from "@material-ui/core";
import React from "react";
import {t} from "../../../locales";

const BaieDeBeauportDescription = () => (
    <Typography variant="body2">
        {t("beauport_bay.description_text_1")} <br/><br/>
        {t("beauport_bay.description_text_2")} <br/><br/>
        {t("beauport_bay.description_text_3")} <br/><br/>
        {t("beauport_bay.description_text_4")} <br/><br/>
    </Typography>
);

export default BaieDeBeauportDescription;
