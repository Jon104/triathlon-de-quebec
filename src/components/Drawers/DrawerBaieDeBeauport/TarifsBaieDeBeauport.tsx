import {Paper, Table, TableBody, TableCell, TableHead, TableRow, Theme, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import React from "react";
import {t} from "../../../locales";

// Catégories	DISTANCES	"Pré-inscrip.
// 15 juillet au 15 août"	"Inscriptions
// 16 août au 25 août"	"Surplace
// Indéterminer"	"Frais membre TQ
// Obligatoire"	NB places
interface Data {
    category: string;
    dayOfTheEvent: string;
    distances: string;
    memberFee: string;
    numberOfPlaces: string;
    preRegistration: string;
    registration: string;
}

const data2: Data[] = [
    {
        category: "Initiation (12 à 17 ans)",
        dayOfTheEvent: "Indéterminer",
        distances: "375m/10km/2,5km",
        memberFee: "7",
        numberOfPlaces: "40",
        preRegistration: "75",
        registration: "85",
    },
    {
        category: "Super Sprint (16+) H&F",
        dayOfTheEvent: "Indéterminer",
        distances: "375m/10km/2,5km",
        memberFee: "20",
        numberOfPlaces: "80",
        preRegistration: "85",
        registration: "95",
    },
    {
        category: "Sprint (16+) H&F",
        dayOfTheEvent: "Indéterminer",
        distances: "750m/20km/5km",
        memberFee: "20",
        numberOfPlaces: "80",
        preRegistration: "95",
        registration: "105",
    },
    {
        category: "Sprint Draft (18+) H&F",
        dayOfTheEvent: "Indéterminer",
        distances: "750m/20km/5km",
        memberFee: "20",
        numberOfPlaces: "80",
        preRegistration: "110",
        registration: "120",
    },
    {
        category: "Equipe Sprint (14+)",
        dayOfTheEvent: "Indéterminer",
        distances: "750m/20km/5km",
        memberFee: "20",
        numberOfPlaces: "20",
        preRegistration: "120",
        registration: "140",
    },
];

const data = [
    {
        price: "50$",
        title: "Prévente",
    },
    {
        price: "55$",
        title: "1 au 9 août",
    },
    {
        price: "60$",
        title: "10 au 12 août",
    },
    {
        price: "-",
        title: "Aucune inscription sur place le jour de l’événement.",
    },
];

const useStyles = makeStyles((theme: Theme) => ({
    tableRightBorder: {
        borderColor: "black",
        borderStyle: "solid",
        borderWidth: 1,
    },
}));

const TarifsBaieDeBeauport = () => {
    const classes = useStyles();
    return (
        <Paper>

            <Typography variant="subtitle1">Inscription (à venir)</Typography>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell align="center" className={classes.tableRightBorder}>Catégories</TableCell>
                        <TableCell align="center" className={classes.tableRightBorder}>Distances</TableCell>
                        <TableCell align="center" className={classes.tableRightBorder}>Pré-inscrip.
                            15 juillet au 15 août</TableCell>
                        <TableCell align="center" className={classes.tableRightBorder}>Inscriptions
                            16 août au 25 août</TableCell>
                        <TableCell align="center" className={classes.tableRightBorder}>Surplace
                            Indéterminer</TableCell>
                        <TableCell align="center" className={classes.tableRightBorder}>Frais
                            membre TQ
                            Obligatoire</TableCell>
                        <TableCell align="center" className={classes.tableRightBorder}>NB places</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data2.map((dat, i) => (
                        <TableRow key={i}>
                            <TableCell align="center" className={classes.tableRightBorder}>{dat.category}</TableCell>
                            <TableCell align="center" className={classes.tableRightBorder}>{dat.distances}</TableCell>
                            <TableCell
                                align="center"
                                className={classes.tableRightBorder}
                            >
                                {dat.preRegistration} $
                            </TableCell>
                            <TableCell
                                align="center"
                                className={classes.tableRightBorder}
                            >
                                {dat.registration} $
                            </TableCell>
                            <TableCell
                                align="center"
                                className={classes.tableRightBorder}
                            >
                                {dat.dayOfTheEvent}
                            </TableCell>
                            <TableCell align="center" className={classes.tableRightBorder}>{dat.memberFee} $</TableCell>
                            <TableCell
                                align="center"
                                className={classes.tableRightBorder}
                            >
                                {dat.numberOfPlaces}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    );
};

export default TarifsBaieDeBeauport;
