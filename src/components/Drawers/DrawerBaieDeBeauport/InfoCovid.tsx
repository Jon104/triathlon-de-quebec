import React from "react";

const BASE_URL = process.env.PUBLIC_URL;
const InfoCovid = () => (
    <>
        <ul>
            <li><a href={`${BASE_URL}/doc/POLITIQUE D’ANNULATION 2021.pdf`}> Politique d’annulation ou de remboursement
            </a></li>
            <li><a href={`${BASE_URL}/doc/ResponsabilitéCOVID.pdf`} target="_blank">Responsabilité et protection
                individuelle TQ - COVID19</a></li>
            <li>Reports 2020 à 2021
                Pour les participants de l’édition 2020: qui ont opté pour le report d’inscription à l’édition 2021,
                veuillez communiquer à l’adresse suivante: triathlondequebec@gmail.com
            </li>
            <li>
                <a href={`${BASE_URL}/doc/21-098-09_napperon_loisir-sport-ete2021_v16.pdf`}>Napperon Loisirs et
                    Sports</a>
            </li>
        </ul>
    </>
);
export default InfoCovid;
