import {Grid, Paper, Theme, Typography, withStyles} from "@material-ui/core";
import React from "react";
import { t } from "../../../locales";
import Joanie from "../Ambassadeurs/AmbassadeurJoanie";

interface MyProps {
    classes: any;
}

const styles = (theme: Theme) => ({
    paper: {
        color: theme.palette.text.secondary,
        padding: theme.spacing(2),
    },
    root: {
        width: "100%",
    },
    typography: {
        padding: "20px",
    },
});

class AmbassadeursBaieDeBeauport extends React.Component<MyProps> {
    public render() {
        const {classes} = this.props;

        return (
            <Grid container={true} className={classes.root} spacing={8}>
                <Grid item={true} xs={12} sm={6} className={classes.items}>
                    <Paper className={classes.paper}>
                        <Typography variant="subtitle1">{t("ambassadors.bruno")}</Typography>
                        <Typography variant="body2" className="basic-padding-bottom">
                            {t("ambassadors.brunoTitle")}
                        </Typography>
                        <img
                            src={require("../../../assets/bruno.png")}
                            alt="image Bruno Savard"
                            className="parcours-ste-foy basic-padding-bottom"
                        />
                        <Grid item={true} justify-content="center">
                            <Typography variant="body1">{t("ambassadors.brunoText")}</Typography>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid item={true} xs={12} sm={6}>
                    <Joanie/>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(AmbassadeursBaieDeBeauport);
