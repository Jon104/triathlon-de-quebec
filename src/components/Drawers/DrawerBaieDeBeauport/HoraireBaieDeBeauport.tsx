import { Typography } from "@material-ui/core";
import React from "react";

const BASE_URL = process.env.PUBLIC_URL;
const HoraireBaieDeBeauport = () => (
  <Typography variant="body2">
      Samedi, 28 août 2021 AM.
      L’événement sera conclu à 12h00.
  </Typography>
);

export default HoraireBaieDeBeauport;
