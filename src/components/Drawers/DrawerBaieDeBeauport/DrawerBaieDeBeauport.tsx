import {Drawer, Grid, IconButton, Paper, Theme, Typography, withStyles} from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import React from "react";
import { t } from "../../../locales";
import DrawerBaieDeBeauportContent from "./DrawerBaieDeBeauportContent";

interface MyProps {
    open: boolean;
    toggleDrawer: any;
    classes: any;
    theme: any;
}

const styles = (theme: Theme) => ({
    drawerPaper: {
        [theme.breakpoints.down("sm")]: {
            width: "100%",
        },
        [theme.breakpoints.up("md")]: {
            width: "50%",
        },
    },
    partenaireTitle: {
        padding: 20,
    },
    root: {
        bottom: 0,
        margin: 0,
        width: "100%",
    },
});

class DrawerBaieDeBeauport extends React.Component<MyProps> {
    public render() {
        const {classes} = this.props;

        return (
            <Drawer
                anchor="left"
                open={this.props.open}
                onClose={this.props.toggleDrawer("isBaieDeBeauportDrawerOpen", false)}
                classes={{paper: classes.drawerPaper}}
            >
                <div className="drawerHeader">
                    <img
                        src={require("../../../assets/logobaiebeauport.png")}
                        alt="logo beauport in drawer"
                        className="drawer-logo"
                    />
                    <IconButton
                        className="close-drawer"
                        onClick={this.props.toggleDrawer("isBaieDeBeauportDrawerOpen", false)}
                    >
                        <ChevronLeftIcon/>
                    </IconButton>
                </div>

                <div
                    className="baiedebeauport-drawer"
                    tabIndex={0}
                    role="button"
                >
                    <DrawerBaieDeBeauportContent/>
                    <Paper>
                        <Typography variant="h6" className={classes.partenaireTitle}>
                            {t("beauport_bay.partners")}
                        </Typography>
                        <Grid
                            container={true}
                            className={classes.root}
                            justify-content="space-around"
                            alignItems="center"
                            spacing={10}
                        >
                            <Grid item={true} xs={6} className={classes.items}>
                                <img
                                    src={require("../../../assets/boutique_du_lac_logo.png")}
                                    alt="logo boutique du lac"
                                    className="logo"
                                />
                            </Grid>
                            <Grid item={true} xs={6} className={classes.items}>
                                <img
                                    src={require("../../../assets/ultriathlon_large.png")}
                                    width={290}
                                    alt="logo port de quebec"
                                    className="logo"
                                />
                            </Grid>
                            <Grid item={true} xs={12} className={classes.items}>
                                <img
                                    src={require("../../../assets/TQ couleur.png")}
                                    alt="logo tq couleur"
                                    className="logo"
                                />
                            </Grid>
                        </Grid>
                    </Paper>
                </div>
            </Drawer>
        );
    }
}

export default withStyles(styles, {withTheme: true})(DrawerBaieDeBeauport);
