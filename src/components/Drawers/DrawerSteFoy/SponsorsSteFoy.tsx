import {GridList, GridListTile} from "@material-ui/core";

import React from "react";

const SponsorsSteFoy: React.FC = () => {
    return (
        <GridList cols={2}>
            <GridListTile>
                <img
                    src={require("../../../assets/TQ couleur.png")}
                    alt="logo triathlon de quebec"
                    className="logo"
                />
            </GridListTile>
            <GridListTile>
                <img
                    src={require("../../../assets/ultriathlon_large.png")}
                    alt="logo triathlon ul"
                    className="logo"
                />
            </GridListTile>
            <GridListTile>
                <img
                    src={require("../../../assets/boutique_du_lac_logo.svg")}
                    alt="logo boutique du lac"
                    className="logo"
                />
            </GridListTile>
        </GridList>
    );
};

export default SponsorsSteFoy;
