import {Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import React from "react";
import {t} from "../../../locales";

interface TimeDetail {
    id: number;
    heure: string;
    detail: string;
}

let id = 0;
const createData = (heure: string, detail: string): TimeDetail => {
    id += 1;
    return {id, heure, detail};
};

const HoraireSteFoy = () => {
    const beforeRaceDayRows =  [
        createData("16h à 19h", t("stefoy.horaire.beforeRaceDay.remiseDesDossards")),
    ];

    const raceDayRows = [
        createData("Dès 5 h 30", t("stefoy.horaire.raceDay.remiseDesDossards")),
        createData("7h15", t("stefoy.horaire.raceDay.firstStart")),
        createData("7h17", t("stefoy.horaire.raceDay.secondStart")),
        createData("7h45", t("stefoy.horaire.raceDay.thirdStart")),
        createData("9h00", t("stefoy.horaire.raceDay.fourthStart")),
        createData("9h02", t("stefoy.horaire.raceDay.fifthStart")),
        createData("9h30", t("stefoy.horaire.raceDay.sixStart")),
        createData("9h32", t("stefoy.horaire.raceDay.sevenStart")),
        createData("10h00", t("stefoy.horaire.raceDay.firstAward")),
        createData("10h45", t("stefoy.horaire.raceDay.eigthStart")),
        createData("10h47", t("stefoy.horaire.raceDay.ninthStart")),
        createData("12h00", t("stefoy.horaire.raceDay.secondAward")),
        createData("12h20", t("stefoy.horaire.raceDay.tenthStart")),
        createData("12h35", t("stefoy.horaire.raceDay.elevenStart")),
        createData("13h00", t("stefoy.horaire.raceDay.twelveStart")),
    ];
    const createRows = (rows: TimeDetail[]) => (
        rows.map((row) => (
            <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                    {row.heure}
                </TableCell>
                <TableCell align="left">{row.detail}</TableCell>
            </TableRow>
        )));
    return (
      <Paper>
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>{t("stefoy.horaire.beforeRaceDay.date")}</TableCell>
                    <TableCell/>
                </TableRow>
            </TableHead>
            <TableBody>
                {createRows(beforeRaceDayRows)}
            </TableBody>
            <TableHead>
                <TableRow>
                    <TableCell>{t("stefoy.horaire.raceDay.date")}</TableCell>
                    <TableCell/>
                </TableRow>
            </TableHead>
            <TableBody>
                {createRows(raceDayRows)}
            </TableBody>
        </Table>
      </Paper>
    );
};

export default HoraireSteFoy;
