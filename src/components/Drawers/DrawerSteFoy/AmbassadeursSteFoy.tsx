import {Grid, Theme} from "@material-ui/core";
import {withStyles} from "@material-ui/core/styles";
import React from "react";
import Joanie from "../Ambassadeurs/AmbassadeurJoanie";

interface MyProps {
    classes: any;
}

const styles = (theme: Theme) => ({
    paper: {
        color: theme.palette.text.secondary,
        padding: theme.spacing(2),
    },
    root: {
        width: "100%",
    },
    typography: {
        padding: "20px",
    },
});

class AmbassadeursBaieDeBeauport extends React.Component<MyProps> {
    public render() {
        const {classes} = this.props;

        return (
            <Grid container={true} className={classes.root} spacing={10}>
                <Grid item={true} xs={12} sm={6} />
            </Grid>
        );
    }
}

export default withStyles(styles)(AmbassadeursBaieDeBeauport);
