import {Typography} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import React from "react";
import { t } from "../../../locales";

interface Data {
  category: string;
  preRegistration: number;
  registration: number;
  dayOfTheEvent: number;
  memberFee: number;
}

const createData = (
  category: string,
  preRegistration: number,
  registration: number,
  dayOfTheEvent: number,
  memberFee: number,
): Data => {
  return { category, preRegistration, registration, dayOfTheEvent, memberFee };
};

function TarifsSteFoy() {
  const rows = [
    createData(t("stefoy.rate_section.categories.U5_U7"), 25, 25, 30, 7),
    createData(t("stefoy.rate_section.categories.U7_U9"), 30, 30, 35, 7),
    createData(t("stefoy.rate_section.categories.U9_U11"), 35, 35, 40, 7),
    createData(t("stefoy.rate_section.categories.U11_U13"), 35, 35, 40, 7),
    createData(t("stefoy.rate_section.categories.U13"), 40, 45, 50, 7),
    createData(t("stefoy.rate_section.categories.U15"), 40, 45, 50, 7),
    createData(t("stefoy.rate_section.categories.quebec_game"), 65, 75, 80, 7),
    createData(
      t("stefoy.rate_section.categories.initiation_triathlon"),
      55,
      60,
      80,
      7,
    ),
    createData(t("stefoy.rate_section.categories.duathlon"), 90, 100, 120, 20),
    createData(
      t("stefoy.rate_section.categories.sprint_triathlon"),
      95,
      105,
      125,
      20,
    ),
    createData(
      t("stefoy.rate_section.categories.olympic_triathlon"),
      105,
      120,
      140,
      20,
    ),
    createData(
      t("stefoy.rate_section.categories.team_sprint_triathlon"),
      120,
      135,
      170,
      20,
    ),
    createData(
      t("stefoy.rate_section.categories.team_olympic_triathlon"),
      140,
      155,
      180,
      20,
    ),
  ];
  const createRows = () => {
    return rows.map((row, id) => (
      <TableRow key={id}>
        <TableCell component="th" scope="row">
          {row.category}
        </TableCell>
        <TableCell align="center">{row.preRegistration}</TableCell>
        <TableCell align="center">{row.registration}</TableCell>
        <TableCell align="center">{row.dayOfTheEvent}</TableCell>
        <TableCell align="center" className="grey-background">
          {row.memberFee}
        </TableCell>
      </TableRow>
    ));
  };

  return (
    <>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                {t("stefoy.rate_section.table_head.column_0")}
              </TableCell>
              <TableCell align="center">
                {t("stefoy.rate_section.table_head.column_1")}
              </TableCell>
              <TableCell align="center">
                {t("stefoy.rate_section.table_head.column_2")}
              </TableCell>
              <TableCell align="center">
                {t("stefoy.rate_section.table_head.column_3")}
              </TableCell>
              <TableCell align="center" className="grey-background">
                <Tooltip
                  title={t("stefoy.rate_section.table_head.column_4_tooltip")}
                >
                  <span>{t("stefoy.rate_section.table_head.column_4")}</span>
                </Tooltip>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{createRows()}</TableBody>
        </Table>
        <Typography variant="body1" className="basic-padding">
          ● {t("stefoy.rate_section.table_helper.helper_1")}
        </Typography>
        <Typography variant="h6" className="basic-padding">
          {t("stefoy.cancelling_term")}
        </Typography>
        <Typography variant="body1" className="basic-padding">
          {t("stefoy.cancelling_term_body")}
        </Typography>
      </Paper>
    </>
  );
}

export default TarifsSteFoy;
