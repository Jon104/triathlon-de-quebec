import React from "react";

const BASE_URL = process.env.PUBLIC_URL;
const Covid19Section: React.FC = () => {
    return (
        <ul>
            <li>
                <a
                    href={`${BASE_URL}/doc/Communiqué_12 mars V4.pdf`}
                    target="_blank"
                >
                    Communiqué SPORTSQUÉBEC et RSEQ
                </a>
            </li>
            <li>Politique d’annulation ou de remboursement <br/>
                (À venir)
            </li>
            <li>
                <a
                    href={`${BASE_URL}/doc/Responsabilité et protection individuelle TQ - COVID19.pdf`}
                    target="_blank"
                >
                    Responsabilité et protection individuelle TQ - COVID19
                </a>
            </li>
            <li>
                Reports 2020 à 2021 <br/>
                Pour les participants de l’édition 2020 qui ont opté pour le report d’inscription à l’édition 2021,
                veuillez communiquer à l’adresse suivante: triathlondequebec@gmail.com
            </li>
        </ul>
    );
};

export default Covid19Section;
