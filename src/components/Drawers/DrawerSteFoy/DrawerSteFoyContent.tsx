import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  List,
  ListItem,
  Typography,
} from "@material-ui/core";
import React from "react";
import { t } from "../../../locales";
import Covid19Section from "./Covid19Section";
import SponsorsSteFoy from "./SponsorsSteFoy";

class DrawerSteFoyContent extends React.Component {
  public render() {
    return (
      <React.Fragment>
        <ExpansionPanel defaultExpanded={true}>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.description")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography variant="body1">{t("stefoy.descriptionText")}</Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.liabilityAndCovid19")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Covid19Section />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.rate")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography variant="h6">À venir</Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.schedule")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography variant="h6">À venir</Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.eventMap")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography variant="h6">À venir</Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.course")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography variant="h6">À venir</Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.guide")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography variant="h6">À venir</Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.preRaceMeeting")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography variant="h6">À venir</Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.results")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <List>
              <ListItem>
                <a
                    href="https://www.sportstats.ca/display-results.xhtml?raceid=104764"
                    target="_blank"
                >
                  <Typography>2019</Typography>
                </a>
              </ListItem>
              <ListItem>
                <a
                    href="https://www.sportstats.ca/display-results.xhtml?raceid=93480"
                    target="_blank"
                >
                  <Typography>2018</Typography>
                </a>
              </ListItem>
              <ListItem>
                <a
                    href="https://www.sportstats.ca/display-results.xhtml?raceid=42253"
                    target="_blank"
                >
                  <Typography>2017</Typography>
                </a>
              </ListItem>
              <ListItem>
                <a
                    href="https://www.sportstats.ca/display-results.xhtml?raceid=30178"
                    target="_blank"
                >
                  <Typography>2016</Typography>
                </a>
              </ListItem>
              <ListItem>
                <a
                    href="https://www.sportstats.ca/display-results.xhtml?raceid=23825"
                    target="_blank"
                >
                  <Typography>2015</Typography>
                </a>
              </ListItem>
            </List>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.faq")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography variant="h6">À venir</Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant="h6">{t("stefoy.sponsors")}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <SponsorsSteFoy />
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </React.Fragment>
    );
  }
}

export default DrawerSteFoyContent;
