import {Drawer, IconButton, Theme, withStyles} from "@material-ui/core";
import {ChevronRight as ChevronRightIcon } from "@material-ui/icons";
import React from "react";
import DrawerSteFoyContent from "./DrawerSteFoyContent";

interface MyProps {
    open: boolean;
    toggleDrawer: any;
    classes: any;
    theme: any;
}

const styles = (theme: Theme) => ({
    drawerPaper: {
        [theme.breakpoints.down("sm")]: {
            width: "100%",
        },
        [theme.breakpoints.up("md")]: {
            width: "50%",
        },
    },
});

class DrawerSteFoy extends React.Component<MyProps> {
    public render() {
        const {classes} = this.props;

        return (
            <Drawer
                anchor="right"
                open={this.props.open}
                onClose={this.props.toggleDrawer("isSteFoyDrawerOpen", false)}
                classes={{paper: classes.drawerPaper}}
            >
                <div className="drawerHeader">
                    <img
                        src={require("../../../assets/logostefoy.png")}
                        alt="logo beauport in drawer"
                        className="drawer-logo"
                    />
                    <IconButton className="close-drawer" onClick={this.props.toggleDrawer("isSteFoyDrawerOpen", false)}>
                        <ChevronRightIcon/>
                    </IconButton>
                </div>

                <div
                    className="baiedebeauport-drawer"
                    tabIndex={0}
                    role="button"
                >
                    <DrawerSteFoyContent/>
                </div>
            </Drawer>
        );
    }
}

export default withStyles(styles, {withTheme: true})(DrawerSteFoy);
