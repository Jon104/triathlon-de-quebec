import { Typography } from "@material-ui/core";
import React from "react";

const BASE_URL = process.env.PUBLIC_URL;
const HoraireAquathlon = () => (
    <Typography variant="subtitle1">
        Samedi, 5 février 2022.
        Détails des heures de départ à venir.
    </Typography>
);

export default HoraireAquathlon;
