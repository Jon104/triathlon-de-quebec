import { Typography } from "@material-ui/core";
import React from "react";

const ResultatsAquathlon = () => (
    <Typography variant="subtitle1">
        À venir.
        Veuillez nous accorder un délai de 48h pour la diffusion des résultats.
    </Typography>
);

export default ResultatsAquathlon;
