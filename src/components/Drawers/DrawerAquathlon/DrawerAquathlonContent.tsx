import {
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary,
    Typography,
    withStyles,
} from "@material-ui/core";
import React from "react";
import {t} from "../../../locales";
import AquathlonDescription from "./AquathlonDescription";
import GuideAthleteAquathlon from "./GuideAthleteAquathlon";
import HoraireAquathlon from "./HoraireAquathlon";
import InfoCovidAquathlon from "./InfoCovidAquathlon";
import ParcoursAquathlon from "./ParcoursAquathlon";
import ResultatsAquathlon from "./ResultatsAquathlon";
import ReunionAvantCourseAquathlon from "./ReunionAvantCourseAquathlon";
import TarifsAquathlon from "./TarifsAquathlon";

const styles = {
    listItem: {
        padding: "0",
    },
    specialAnnouncement: {
        backgroundColor: "#a90409",
        color: "white",
    },
    specialAnnouncementText: {
        color: "white",
    },
};

interface MyProps {
    classes: any;
}

class DrawerAquathlonContent extends React.Component<MyProps> {

    public render() {
        const {classes} = this.props;

        return (
            <>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("aquathlon.description")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <AquathlonDescription />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("aquathlon.covid")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <InfoCovidAquathlon />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("aquathlon.rate")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <TarifsAquathlon />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("aquathlon.schedule")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <HoraireAquathlon />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("aquathlon.course")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <ParcoursAquathlon />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("aquathlon.athletes_guide")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <GuideAthleteAquathlon />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("aquathlon.pre_race_briefing")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <ReunionAvantCourseAquathlon />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary>
                        <Typography variant="h6">{t("aquathlon.results")}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <ResultatsAquathlon />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </>
        );
    }
}

export default withStyles(styles, {withTheme: true})(DrawerAquathlonContent);
