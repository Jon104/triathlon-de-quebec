import React from "react";

const BASE_URL = process.env.PUBLIC_URL;

const InfoCovidAquathlon = () => (
    <ul>
        <li><a href={`${BASE_URL}/doc/Communiqué_12 mars V4.pdf`}>Communiqué SPORTSQUÉBEC et RSEQ</a></li>
        <li>
            <a href={`${BASE_URL}/doc/POLITIQUE D’ANNULATION 2022.pdf`}>Politique d'annulation ou de remboursement</a>
        </li>
        <li>
            <a href={`${BASE_URL}/doc/Responsabilité et protection individuelle TQ - COVID19.pdf`}>
                Responsabilité et protection individuelle TQ - COVID19
            </a>
        </li>
    </ul>
);

export default InfoCovidAquathlon;
