import { Drawer, Grid, IconButton, Paper, Theme, Typography, withStyles} from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import React from "react";
import { t } from "../../../locales";
import DrawerAquathlonContent from "./DrawerAquathlonContent";

interface MyProps {
    open: boolean;
    toggleDrawer: any;
    classes: any;
    theme: any;
}

const styles = (theme: Theme) => ({
    drawerPaper: {
        [theme.breakpoints.down("sm")]: {
            width: "100%",
        },
        [theme.breakpoints.up("md")]: {
            width: "50%",
        },
    },
    partenaireTitle: {
        padding: 20,
    },
    root: {
        bottom: 0,
        margin: 0,
        width: "100%",
    },
});

class DrawerAquathlon extends React.Component<MyProps> {
    public render() {
        const {classes} = this.props;

        return (
            <Drawer
                anchor="left"
                open={this.props.open}
                onClose={this.props.toggleDrawer("isAquathlonDrawerOpen", false)}
                classes={{paper: classes.drawerPaper}}
            >
                <div className="drawerHeader">
                    <img
                        src={require("../../../assets/logoaquathlon2020.png")}
                        alt="logo aquathlon in drawer"
                        className="drawer-logo"
                    />
                    <IconButton
                        className="close-drawer"
                        onClick={this.props.toggleDrawer("isAquathlonDrawerOpen", false)}
                    >
                        <ChevronLeftIcon/>
                    </IconButton>
                </div>

                <div
                    className="aquathlon-drawer"
                    tabIndex={0}
                    role="button"
                >
                    <DrawerAquathlonContent/>
                    <Paper>
                        <Typography variant="h6" className={classes.partenaireTitle}>
                            {t("aquathlon.partners")}
                        </Typography>
                    </Paper>
                </div>
            </Drawer>
        );
    }
}

export default withStyles(styles, {withTheme: true})(DrawerAquathlon);
