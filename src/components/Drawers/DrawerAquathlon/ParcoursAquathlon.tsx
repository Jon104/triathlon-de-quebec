import { Paper, Table, TableBody, TableCell, TableHead, TableRow, Theme, Typography} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";

interface Data {
  category: string;
  distance: string;
  laps: string;
}

const data: Data[] = [
  {
    category: "U13 Gars et Filles",
    distance: "1000m/350m/1000m",
    laps: "5 - 3 - 5",
  },
  {
    category: "U15 Gars",
    distance: "1250m/500m/1250m",
    laps: "6 - 4 - 6",
  },
  {
    category: "U15 Filles",
    distance: "1250m/500m/1250m",
    laps: "6 - 4 - 6",
  },
  {
    category: "U17 Gars",
    distance: "1250m/500m/1250m",
    laps: "6 - 4 - 6",
  },
  {
    category: "U17 Filles",
    distance: "1250m/500m/1250m",
    laps: "6 - 4 - 6",
  },
  {
    category: `Élites Gars/Filles Collégial et\nUniversitaire/GA Compétitifs`,
    distance: "2000m/700m/2000m",
    laps: "10 - 6 - 10",
  },
  {
    category: "GA Récréatifs Hommes/Femme",
    distance: "500m/3000m",
    laps: "4 - 15",
  },
  {
    category: "Relais mixte",
    distance: "4 x (350m/1000m)",
    laps: "4x (3 - 5)",
  },
];

const useStyles = makeStyles((theme: Theme) => ({
  tableRightBorder: {
    borderColor: "black",
    borderStyle: "solid",
    borderWidth: 1,
  },
  tableRightBorderNoWrap: {
      borderColor: "black",
      borderStyle: "solid",
      borderWidth: 1,
      whiteSpace: "nowrap",
  },
}));

const ParcoursAquathlon = () => {
  const classes = useStyles();

  return (
    <Paper>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="center" className={classes.tableRightBorder}>Catégories</TableCell>
            <TableCell align="center" className={classes.tableRightBorder}>Distance</TableCell>
            <TableCell align="center" className={classes.tableRightBorder}>Tours</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((dat, i) => (
            <TableRow key={i}>
              <TableCell align="center" className={classes.tableRightBorder}>{dat.category}</TableCell>
              <TableCell align="center" className={classes.tableRightBorder}>{dat.distance}</TableCell>
              <TableCell align="center" className={classes.tableRightBorderNoWrap}>{dat.laps}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default ParcoursAquathlon;
