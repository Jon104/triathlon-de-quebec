import { Paper, Table, TableBody, TableCell, TableHead, TableRow, Theme, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";

interface Data {
    category: string;
    earlyRate: string;
    regularRate: string;
    dayOfTheEventRate: string;
}

const data: Data[] = [
    {
        category: "Aquathlon U13",
        dayOfTheEventRate: "70$",
        earlyRate: "55$",
        regularRate: "60$",
    },
    {
        category: "Aquathlon U15",
        dayOfTheEventRate: "70$",
        earlyRate: "55$",
        regularRate: "60$",
    },
    {
        category: "Aquathlon U17",
        dayOfTheEventRate: "70$",
        earlyRate: "55$",
        regularRate: "60$",
    },
    {
        category: "Aquathlon Élites,\nCollégial et Universitaire",
        dayOfTheEventRate: "70$",
        earlyRate: "55$",
        regularRate: "60$",
    },
    {
        category: "Aquathlon GA",
        dayOfTheEventRate: "70$",
        earlyRate: "55$",
        regularRate: "60$",
    },
    {
        category: "Relais mixte",
        dayOfTheEventRate: "110$",
        earlyRate: "95$",
        regularRate: "100$",
    },
];

const useStyles = makeStyles((theme: Theme) => ({
    tableRightBorder: {
        borderColor: "black",
        borderStyle: "solid",
        borderWidth: 1,
    },
}));

const TarifsAquathlon = () => {
    const classes = useStyles();

    return (
        <Paper>
            <Typography variant="subtitle1">Ouverture des inscriptions en décembre 2021.</Typography>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell align="center" className={classes.tableRightBorder}>Catégories</TableCell>
                        <TableCell align="center" className={classes.tableRightBorder}>
                            Avant 10 Janvier 23h59
                        </TableCell>
                        <TableCell align="center" className={classes.tableRightBorder}>
                            Avant 31 Janvier 23h59
                        </TableCell>
                        <TableCell align="center" className={classes.tableRightBorder}>Sur place*</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((dat, i) => (
                        <TableRow key={i}>
                            <TableCell align="center" className={classes.tableRightBorder}>{dat.category}</TableCell>
                            <TableCell align="center" className={classes.tableRightBorder}>{dat.earlyRate}</TableCell>
                            <TableCell align="center" className={classes.tableRightBorder}>{dat.regularRate}</TableCell>
                            <TableCell align="center" className={classes.tableRightBorder}>
                                {dat.dayOfTheEventRate}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <Typography variant="subtitle1">Les prix incluent tous les frais et les taxes.</Typography>
            <br />
            <Typography variant="body2">
                <b>*Inscriptions sur place</b> : argent comptant ou par chèque à l'ordre du club de triathlon UL.
            </Typography>
        </Paper>
    );
};

export default TarifsAquathlon;
