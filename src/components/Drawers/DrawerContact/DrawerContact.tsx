import {Drawer, Grid, IconButton, Theme, Typography, withStyles} from "@material-ui/core";
import {Close as CloseIcon} from "@material-ui/icons";
import React from "react";
import {t} from "../../../locales";

interface MyProps {
    open: boolean;
    toggleDrawer: any;
    classes: any;
    theme: any;
}

const styles = (theme: Theme) => ({
    closeButton: {
        align: "right",
        margin: "5px",
    },
    desktopGrid: {
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },
    },
    drawerPaper: {
        backgroundColor: "#000000",
        height: "100%",
        opacity: 0.87,
        width: "100%",
    },
    mobileGrid: {
        [theme.breakpoints.up("md")]: {
            display: "none",
        },
    },
});

class DrawerContact extends React.Component<MyProps> {
    public render() {
        const {classes} = this.props;
        return (
            <Drawer
                anchor="bottom"
                open={this.props.open}
                onClose={this.props.toggleDrawer("isContactSectionOpen", false)}
                transitionDuration={{enter: 1000, exit: 800}}
                classes={{paper: classes.drawerPaper}}
            >
                <div className="text-align-right">
                    <IconButton color="secondary" onClick={this.props.toggleDrawer("isContactSectionOpen", false)}>
                        <CloseIcon/>
                    </IconButton>
                </div>
                <Grid container={true} spacing={10} className={classes.desktopGrid} id="contact-container">
                    <Grid item={true} xs={12}>
                        <Typography variant="h3" color="secondary">
                            {t("contact.reach_us")}
                        </Typography>
                    </Grid>
                    <Grid item={true} xs={12}>
                        <Typography variant="h6" color="secondary">
                            {t("contact.base_de_plein_air_ste_foy")}
                        </Typography>
                        <Typography variant="body1" color="secondary">
                            {t("contact.base_de_plein_air_ste_foy_email")}
                        </Typography>
                        <Typography variant="h6" color="secondary">
                            {t("contact.beauport_bay")}
                        </Typography>
                        <Typography variant="body1" color="secondary">
                            {t("contact.beauport_bay_email")}
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container={true} spacing={10} className={classes.mobileGrid} id="contact-container">
                    <Grid item={true} xs={12}>
                        <Typography variant="h5" color="secondary"> {t("contact.reach_us")}</Typography>
                    </Grid>
                    <Grid item={true} xs={12}>
                        <Typography variant="body2" color="secondary">
                            {t("contact.base_de_plein_air_ste_foy")}
                        </Typography>
                        <Typography variant="body1" color="secondary">
                            {t("contact.base_de_plein_air_ste_foy_email")}
                        </Typography>
                        <Typography variant="body2" color="secondary">
                            {t("contact.beauport_bay")}
                        </Typography>
                        <Typography variant="body1" color="secondary">
                            {t("contact.beauport_bay_email")}
                        </Typography>
                    </Grid>
                </Grid>
            </Drawer>
        );
    }
}

export default withStyles(styles, {withTheme: true})(DrawerContact);
