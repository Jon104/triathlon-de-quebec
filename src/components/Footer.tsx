import {Fab, Typography} from "@material-ui/core";
import EmailIcon from "@material-ui/icons/Email";
import React from "react";
import {t} from "../locales";

interface MyProps {
    toggleDrawer: any;
}

class Footer extends React.Component<MyProps> {
    public render() {
        return (
            <footer className="footer">
                <a className="footer-left" href="https://www.triathlonulaval.ca/" target="_blank">
                    <img
                        src={require("../assets/ultriathlon.png")}
                        alt="UL Triathlon logo"
                        id="ultriathlon-logo"
                        className="some-opacity"
                    />
                    <Typography variant="body1" className="white hide-mobile some-opacity">
                        {t("footer.present_by_rouge_et_or_triathlon")}
                    </Typography>
                </a>
                <div className="contact">
                    <Fab
                        color="primary"
                        aria-label="Add"
                        className="some-opacity"
                        onClick={this.props.toggleDrawer("isContactSectionOpen", true)}
                    >
                        <EmailIcon/>
                    </Fab>
                </div>
            </footer>
        );
    }
}

export default Footer;
