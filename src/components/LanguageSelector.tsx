import {Input, MenuItem, Select} from "@material-ui/core";
import React from "react";
import {useTranslation} from "react-i18next";
import useReactRouter from "use-react-router";

const LanguageSelector = () => {
    const {history} = useReactRouter();
    const {i18n} = useTranslation();
    const allLanguages = () => i18n.languages.map((language, key) =>
        <MenuItem key={key} value={language}>{language}</MenuItem>);

    const handleChange = async (event: React.ChangeEvent<{ value: unknown }>) => {
        try {
            const lang = event.target.value;
            history.push(`/${lang}`);
        } catch (e) {
            // tslint:disable-next-line
            console.error(`error: ${e}`);
        }
    };
    return (
        <Select
            value={i18n.language}
            onChange={handleChange}
            input={<Input />}
        >
            {allLanguages()}
        </Select>);
};

export default LanguageSelector;
