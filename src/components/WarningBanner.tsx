import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import { Alert, AlertTitle } from "@material-ui/lab";
import React from "react";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > * + *": {
        marginTop: theme.spacing(2),
      },
      "width": "100%",
    },
  }),
);

const WarningBanner: React.FC = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const close = () => setOpen(false);

  return (
    <div className={classes.root}>
      <Collapse in={open}>
        <Alert
          severity="warning"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={close}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
        >
          <AlertTitle>ANNONCE IMPORTANTE</AlertTitle>
          Selon les annonces gouvernementales en date du 20 décembre, la tenue de l'événement risque d'être compromise.
          Nous communiquerons avec les participants déjà inscrits.
          Pour les autres athlètes souhaitant potentiellement participer,
          une mise à jour sera émise à la mi-janvier.<br/><br/>
          Restez à l'affût!<br/>Et surtout, prenez soin de vous,<br/><br/><b>- L'équipe UL</b>
        </Alert>
      </Collapse>
    </div>
  );
};

export default WarningBanner;
