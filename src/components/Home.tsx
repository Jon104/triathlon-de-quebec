import { Fab, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";

import { changeLanguage, LanguageType, t } from "../locales";
import DrawerAquathlon from "./Drawers/DrawerAquathlon/DrawerAquathlon";
import DrawerBaieDeBeauport from "./Drawers/DrawerBaieDeBeauport/DrawerBaieDeBeauport";
import DrawerContact from "./Drawers/DrawerContact/DrawerContact";
import DrawerSteFoy from "./Drawers/DrawerSteFoy/DrawerSteFoy";
import Footer from "./Footer";
import WarningBanner from "./WarningBanner";

interface State {
  isAquathlonDrawerOpen: boolean;
  isBaieDeBeauportDrawerOpen: boolean;
  isSteFoyDrawerOpen: boolean;
  isContactSectionOpen: boolean;
}

interface Props {
  language: LanguageType;
}
const Home: React.FunctionComponent<Props> = ({ language }) => {
  const [drawerOpen, setDrawerOpen] = useState({
    isAquathlonDrawerOpen: false,
    isBaieDeBeauportDrawerOpen: false,
    isContactSectionOpen: false,
    isSteFoyDrawerOpen: false,
  });
  useEffect(() => {
    const changeLang = async () => {
      await changeLanguage(language);
    };
    changeLang();
  }, [language]);

  const toggleDrawer = (key: keyof State, open: boolean) => (
    event: React.MouseEvent,
  ) => {
    setDrawerOpen((prevState) => ({
      ...prevState,
      [key]: open,
    }));
  };

  return (
    <>
      <WarningBanner />
      <div className="site-container">
        <video
          autoPlay={true}
          loop={true}
          muted={true}
          id="video-background-beauport"
        >
          <source
            src={require("../assets/triathlonBeauportVideo.mp4")}
            type="video/mp4"
          />
        </video>

        <div
            className="panels panel-left"
        >
          <div className="centered-container">
            <img
              src={require("../assets/logostefoy.png")}
              alt="logo ste-foy"
              className="logo"
              id="logo-ste-foy"
            />
            <Typography variant="h6" id="date-ste-foy">
              {t("home.base_de_plein_air_ste_foy_date")}
            </Typography>
          </div>
        </div>
        <div
            className="panels panel-right"
        >
          <div className="centered-container">
            <img
              src={require("../assets/logoaquathlon2020centered.png")}
              alt="logo aquathlon"
              className="logo"
              id="logo-aquathlon"
            />
            <Typography variant="h6">
              {t("home.aquathlon_date")}
            </Typography>
          </div>
        </div>
      </div>

      <DrawerAquathlon
        open={drawerOpen.isAquathlonDrawerOpen}
        toggleDrawer={toggleDrawer}
      />
      <DrawerSteFoy
        open={drawerOpen.isSteFoyDrawerOpen}
        toggleDrawer={toggleDrawer}
      />
      <DrawerContact
        open={drawerOpen.isContactSectionOpen}
        toggleDrawer={toggleDrawer}
      />
      <Footer toggleDrawer={toggleDrawer} />
    </>
  );
};

export default Home;
