import React from "react";

interface NoMatchProps {
    location: {pathname: string};
}

const NotFound: React.FunctionComponent<NoMatchProps> = ({location}) =>
    <div>
        <h3>
            No match for <code>{location.pathname}</code>
        </h3>
    </div>;

export default NotFound;
