import {MuiThemeProvider} from "@material-ui/core";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import React from "react";
import * as ReactDOM from "react-dom";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import {DEFAULT_LANGUAGE} from "./locales";
import "./styles/app.scss";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#a90409",
        },
        secondary: {
            main: "#ffffff",
        },
    },
    typography: {
        fontFamily: [
            "Barlow",
            "Roboto",
            '"Helvetica Neue"',
            "Arial",
            "sans-serif",
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(","),
    },
});

/* tslint:disable: jsx-no-lambda max-line-length*/
const routing =
    <MuiThemeProvider theme={theme}>
        <Router>
            <div>
                <Switch>
                    <Route exact={true} path={["/", `/${DEFAULT_LANGUAGE}`]} component={() => <Home language={DEFAULT_LANGUAGE}/>}/>
                    <Route exact={true} path="/en" component={() => <Home language="en"/>}/>
                    <Route component={NotFound}/>
                </Switch>
            </div>
        </Router>
    </MuiThemeProvider>;
/* tslint:enable: jsx-no-lambda*/
ReactDOM.render(routing, document.getElementById("root") as HTMLElement);
