import translationEN from "./en/index.json";
import translationFR from "./fr/index.json";

export {translationFR, translationEN};
