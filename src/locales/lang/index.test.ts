import {translationEN, translationFR} from ".";

test("it contains all the translations", () => {
    const enKeys = flattenObject(translationEN);
    const frKeys = flattenObject(translationFR);

    Object.keys(enKeys).map((key) => {
        expect(frKeys).toHaveProperty([`${key}`]);
    });
    Object.keys(frKeys).map((key) => {
        expect(enKeys).toHaveProperty([`${key}`]);
    });
});

const flattenObject = (obj: any, prefix = "") =>
    Object.keys(obj).reduce((acc, k) => {
        const pre = prefix.length ? prefix + "." : "";
        if (typeof obj[k] === "object") {
            Object.assign(acc, flattenObject(obj[k], pre + k));
        } else {
            // @ts-ignore
            acc[pre + k] = obj[k];
        }
        return acc;
    }, {});
