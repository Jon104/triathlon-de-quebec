import i18n from "i18next";
import {initReactI18next} from "react-i18next";
import {translationEN, translationFR} from "./lang";

export const DEFAULT_LANGUAGE = "fr";
const SECONDARY_LANGUAGE = "en";
const resources = {
    en: {
        translation: translationEN,
    },
    fr: {
        translation: translationFR,
    },
};

i18n
    .use(initReactI18next)
    .init({
        fallbackLng: [DEFAULT_LANGUAGE, SECONDARY_LANGUAGE],
        interpolation: {
            escapeValue: false,
        },
        lng: DEFAULT_LANGUAGE,
        resources,
    });

export type LanguageType = "en" | "fr";
export const t = (key: string) => i18n.t(key);
export const changeLanguage = (lang: LanguageType) => i18n.changeLanguage(lang);
